reset

set terminal epslatex color
set xlabel 'Ablenkwinkel $\theta$ [$^\circ$]'
set ylabel "Zählrate [$\\frac{\\text{Impulse}}{\\text{s}}$]"
set output 'charStrahl_plot.tex'

p 'charStrahl.txt' u 1:2 t 'Messwerte'

set output
!epstopdf charStrahl_plot.eps

set output 'peaks.tex'
set key left
p [8:11] 'verSpann.txt' u 1:2 t '$U_A$ 23\si{\kilo\volt}', 'verSpann.txt' u 1:3 t '$U_A$ 26\si{\kilo\volt}', 'verSpann.txt' u 1:4 t '$U_A$ 29\si{\kilo\volt}', 'verSpann.txt' u 1:5 t '$U_A$ 32\si{\kilo\volt}', 'verSpann.txt' u 1:6 t '$U_A$ 35\si{\kilo\volt}'

set output
!epstopdf peaks.eps

set output 'brems.tex'
set key left
p [2:8] 'verSpann.txt' u 1:2 t '$U_A$ 23\si{\kilo\volt}', 'verSpann.txt' u 1:3 t '$U_A$ 26\si{\kilo\volt}', 'verSpann.txt' u 1:4 t '$U_A$ 29\si{\kilo\volt}', 'verSpann.txt' u 1:5 t '$U_A$ 32\si{\kilo\volt}', 'verSpann.txt' u 1:6 t '$U_A$ 35\si{\kilo\volt}'

set output
!epstopdf brems.eps

f(x)=a*x+b
g(x)=c*x+d

set fit logfile 'beta.log'
fit f(x) 'intensi.txt' u (($1-19.599)**(3/2)):2 via a,b
set fit logfile 'alpha.log'
fit g(x) 'intensi.txt' u (($1-19.599)**(3/2)):3 via c,d

set xlabel '$(U_A-U_K)^{3/2}$ [\si{\kilo\volt}]'

set output 'inten.tex'

p f(x) t 'Regression', g(x) t 'Regression', 'intensi.txt' u (($1-19.599)**(3/2)):2 t 'Messwerte K$_\beta$', 'intensi.txt' u (($1-19.599)**(3/2)):3 t 'Messwerte K$_\alpha$'

set output
!epstopdf inten.eps

set key right
set xlabel 'Ablenkwinkel $\theta$ [$^\circ$]'
set ylabel '$\ln(R)$'
set output 'niku.tex'

p "AbsorbNickel.txt" u 1:(log($2)) t "Nickel", "AbsorbKupfer.txt" u 1:(log($2)) t "Kupfer"

set output
!epstopdf niku.eps

set ylabel "Zählrate [$\\frac{\\text{Impulse}}{\\text{s}}$]"
set output 'abskurz.tex'

p "kurzAbsorb.txt" t 'kein Absorber', "kurzAbsorbAL.txt" t 'AL', "kurzAbsorbNI.txt" t 'NI', "kurzAbsorbSN.txt" t 'SN', "kurzAbsorbZN.txt" t 'ZN'

set output
!epstopdf abskurz.eps

h(x)=z*x+y
i(x)=w*x+u
fit f(x) "kurzAbsorbAL.txt" u ((2*201*sin($1*pi/180))**3):((log($3/$2)/0.025)/2.7) via a,b
fit g(x) "kurzAbsorbNI.txt" u ((2*201*sin($1*pi/180))**3):((log($3/$2)/0.025)/8.9) via c,d
fit h(x) "kurzAbsorbSN.txt" u ((2*201*sin($1*pi/180))**3):((log($3/$2)/0.025)/7.28) via z,y
fit i(x) "kurzAbsorbZN.txt" u ((2*201*sin($1*pi/180))**3):((log($3/$2)/0.025)/7.14) via w,u


set xlabel '$\lambda^3 [\si{\pico\meter}^3]$'
set ylabel '$\mu/\rho$'
set key left
set output 'abslog.tex'

p f(x) lt -1 notitle, g(x) lt -1 notitle, h(x) lt -1 notitle, i(x) lt -1 notitle, "kurzAbsorbAL.txt" u ((2*201*sin($1*pi/180))**3):((log($3/$2)/0.025)/2.7) t 'AL', "kurzAbsorbNI.txt" u ((2*201*sin($1*pi/180))**3):((log($3/$2)/0.025)/8.9) lt 1 t 'NI', "kurzAbsorbSN.txt" u ((2*201*sin($1*pi/180))**3):((log($3/$2)/0.025)/7.28) t 'SN', "kurzAbsorbZN.txt" u ((2*201*sin($1*pi/180))**3):((log($3/$2)/0.025)/7.14) t 'ZN'

set output
!epstopdf abslog.eps
