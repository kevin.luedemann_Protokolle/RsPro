\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\contentsline {section}{\numberline {2}Theorie}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Spektrum der R\IeC {\"o}ntgenstrahlung}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Bragg-Reflektion}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Absorption}{2}{subsection.2.3}
\contentsline {section}{\numberline {3}Durchf\IeC {\"u}hrung}{3}{section.3}
\contentsline {section}{\numberline {4}Auswertung}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}Charakteristische Spannung}{4}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Bremsstrahlung}{6}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Intensit\IeC {\"a}t der charakteristischen Linien}{7}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Absobtion von Nickel und Kufper}{8}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Absorbtionskoeffizient}{9}{subsection.4.5}
\contentsline {section}{\numberline {5}Diskussion}{10}{section.5}
\contentsline {section}{\nonumberline Literatur}{11}{section*.14}
