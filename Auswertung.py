# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 09:33:45 2015

@author: kevin.luedemann
"""

import maabara as ma
import scipy as sc
import numpy as np
import matplotlib.pyplot as plt
import math as mt

#Unterprogramme
def extrema(array, k):
    c=np.array([[0],[0]])
    minmax=1
    if array[0,k]<array[1,k]:
        minmax=-1
        
    for i in range(len(array)):
        a=minmax*array[i-1,k]
        b=minmax*array[i,k]
        if a>b:
            minmax*=-1
            c=np.append(c,np.array([[array[i,0]],[abs(a)]]), axis = 0)
            
    return c

#Charakterisctoscher Strahl bearbeiten
charstrahl = np.genfromtxt("charStrahlASCI_ohne.txt", delimiter="\t")
#print charstrahl
for i in range(len(charstrahl)):
    charstrahl[i,1]=charstrahl[i,1]/(1-100*charstrahl[i,1]*10**(-6))
    charstrahl[i,0]/=2
out = open("charStrahl.txt","w")
for i in range(len(charstrahl)):
    out.write(str(charstrahl[i,0])+ "\t" + str(charstrahl[i,1]) + "\n")

out.close
#maxchar = extrema(charstrahl,1)
#print maxchar
#plt.errorbar(maxchar[:,0],maxchar[:,1],label='Messwerte', fmt='g.')
#print charstrahl

#Verschiedene Spannungen bearbeiten
verspann = np.genfromtxt("3verSpannungenasci_ohne.txt", delimiter="\t")
#print verspann

for i in range(len(verspann)):
    verspann[i,0]/=2
    for k in range(5):
        verspann[i,k+1]/=(1-100*verspann[i,k+1]*10**(-6))

out = open("verSpann.txt","w")
for i in range(len(verspann)):
    out.write(str(verspann[i,0])+ "\t" + str(verspann[i,1])+ "\t" + str(verspann[i,2])+ "\t" + str(verspann[i,3])+ "\t" + str(verspann[i,4])+ "\t" + str(verspann[i,5]) + "\n")
out.close

abskupf = np.genfromtxt("AKA_o.txt", delimiter="\t")
for i in range(len(abskupf)):
    abskupf[i,0]/=2
    abskupf[i,1]/=(1-100*abskupf[i,1]*10**(-6))
out = open("AbsorbKupfer.txt","w")
for i in range(len(abskupf)):
    out.write(str(abskupf[i,0])+ "\t" + str(abskupf[i,1]) + "\n")

out.close

absnickel = np.genfromtxt("ANA_o.txt", delimiter="\t")
print absnickel
for i in range(len(absnickel)):
    absnickel[i,0]/=2
    absnickel[i,1]/=(1-100*absnickel[i,1]*10**(-6))
out = open("AbsorbNickel.txt","w")
for i in range(len(absnickel)):
    out.write(str(absnickel[i,0])+ "\t" + str(absnickel[i,1]) + "\n")

out.close    
    
kabs = np.genfromtxt("KAA_o.txt", delimiter="\t")
for i in range(len(kabs)):
    kabs[i,0]/=2
    kabs[i,1]/=(1-100*kabs[i,1]*10**(-6))
out = open("kurzAbsorb.txt","w")
for i in range(len(kabs)):
    out.write(str(kabs[i,0])+ "\t" + str(kabs[i,1]) + "\n")

out.close    

kabsAL = np.genfromtxt("KAALA_o.txt", delimiter="\t")
for i in range(len(kabsAL)):
    kabsAL[i,0]/=2
    kabsAL[i,1]/=(1-100*kabsAL[i,1]*10**(-6))
out = open("kurzAbsorbAL.txt","w")
for i in range(len(kabsAL)):
    out.write(str(kabsAL[i,0])+ "\t" + str(kabsAL[i,1]) + "\t" + str(kabs[i,1]) + "\n")

out.close    
    

kabsNI = np.genfromtxt("KANIA_o.txt", delimiter="\t")
for i in range(len(kabsNI)):
    kabsNI[i,0]/=2
    kabsNI[i,1]/=(1-100*kabsNI[i,1]*10**(-6))
out = open("kurzAbsorbNI.txt","w")
for i in range(len(kabsNI)):
    out.write(str(kabsNI[i,0])+ "\t" + str(kabsNI[i,1]) +  "\t" + str(kabs[i,1]) +"\n")

out.close    
    

kabsSN = np.genfromtxt("KASNA_o.txt", delimiter="\t")
for i in range(len(kabsSN)):
    kabsSN[i,0]/=2
    kabsSN[i,1]/=(1-100*kabsSN[i,1]*10**(-6))
out = open("kurzAbsorbSN.txt","w")
for i in range(len(kabsSN)):
    out.write(str(kabsSN[i,0])+ "\t" + str(kabsSN[i,1]) +  "\t" + str(kabs[i,1]) +"\n")

out.close
    
    

kabsZN = np.genfromtxt("KAZNA_o.txt", delimiter="\t")
for i in range(len(kabsZN)):
    kabsZN[i,0]/=2
    kabsZN[i,1]/=(1-100*kabsZN[i,1]*10**(-6))

out = open("kurzAbsorbZN.txt","w")
for i in range(len(kabsZN)):
    out.write(str(kabsZN[i,0])+ "\t" + str(kabsZN[i,1]) +  "\t" + str(kabs[i,1]) +"\n")

out.close
    
    


"""
plt.xlabel('Position $\\theta$ [$^\\circ$]')
plt.ylabel('Impulse [$s^{-1}$]')
plt.errorbar(charstrahl[:,0],charstrahl[:,1],label='Messwerte', fmt='g.')
"""
